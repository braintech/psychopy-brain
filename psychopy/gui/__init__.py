#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Part of the PsychoPy library
# Copyright (C) 2002-2018 Jonathan Peirce (C) 2019 Open Science Tools Ltd.
# Distributed under the terms of the GNU General Public License (GPL).

"""
Graphical user interface elements for experiments.

This lib will attempt to use PyQt (4 or 5) if possible and will revert to
using wxPython if PyQt is not found.
"""

from __future__ import absolute_import, print_function
import sys
from .. import constants

# check if wx has been imported. If so, import here too and check for app
if 'wx' in sys.modules:
    import wx
    wxApp = wx.GetApp()
else:
    wxApp = None

# then setup prefs for

haveQt = False  # until we find otherwise
if wxApp is None:  # i.e. don't try this if wx is already running
    try:
        import PySide2
        haveQt = True
    except ImportError:
        haveQt = False

# now we know what we can import let's import the rest
if haveQt:
    from .qtgui import *
else:
    try:
        from .wxgui import *
    except ImportError:
        print("Neither wxPython nor PyQt could be imported "
              "so gui is not available")
