#!/usr/bin/env python3
"""Python3 compatible script for compiling .psyexp files to python scripts."""
from psychopy.experiment import Experiment


def compile_psyexp_to_python_script(psyexp_path, py_path=None):
    experiment = Experiment()
    experiment.loadFromXML(psyexp_path)
    script_buffer = experiment.writeScript(py_path)
    if py_path is None:
        print(script_buffer)
    else:
        with open(py_path, 'w', encoding='utf8') as py_script:
            py_script.write(script_buffer)


def main():
    import argparse

    parser = argparse.ArgumentParser(
        description='Compile psychopy experiment file to python3 script.'
    )
    parser.add_argument(
        'input',
        help='path to psychopy experiment file (*.psyexp)'
    )
    parser.add_argument(
        '-o', '--output',
        help='path to compiled python3 (*.py)'
    )
    args = parser.parse_args()

    compile_psyexp_to_python_script(args.input, args.output)


if __name__ == '__main__':
    main()
