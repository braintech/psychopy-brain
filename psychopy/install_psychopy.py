# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved
import os
import subprocess
import sys
from ctypes import cast, c_char_p
from pathlib import Path

base = Path(__file__).parent.parent
resources_path = (Path(__file__).parent / 'app' / "Resources").relative_to(base)
LINKS = [
    (resources_path / 'psychopy.desktop', '/usr/share/applications/'),
    (resources_path / 'psychopy.png', '/usr/share/pixmaps/'),
    (resources_path / '99-ZFTDI.rules', '/etc/udev/rules.d/'),
]
APT_REQUIREMENTS = [
    'ffmpeg',
    'git',
    'libsndfile1',
    'libportaudio2',
]

intel_fix_file = (Path(__file__).parent / 'app' / "Resources" / "20-intel.conf").absolute()
intel_fix_target = '/etc/X11/xorg.conf.d/20-intel.conf'


def _apt_to_depends(requirements):
    reqs = []
    for req in requirements:
        if '=' in req:
            req = "{}(={})".format(*req.split('='))
        reqs.append(req)
    return reqs


def install_apt_requirements(apt_requirements, package_name):
    if '--print-apt-requirements' in sys.argv:
        print("\n".join(apt_requirements))
        sys.exit(0)
    if not apt_requirements:
        return
    elif 'GATHER_APT_REQUIREMENTS' in os.environ:
        with open(os.environ['GATHER_APT_REQUIREMENTS'], 'a') as f:
            f.write("\n".join(_apt_to_depends(apt_requirements)) + '\n')
    elif os.name == 'posix':
        cmd = "apt-get install -y " + " ".join(['"%s"' % req for req in apt_requirements])
        cmd = add_sudo_if_nonroot(cmd)
        print("Installing %s apt dependencies: \n %s" % (
            package_name, cmd))  # Sadly this is only visible when pip is run with '-v'
        if '--dry-run' in sys.argv:
            return
        os.system(cmd)


def add_sudo_if_nonroot(cmd):
    if os.geteuid() != 0:
        return "sudo " + cmd
    return cmd


def create_links(links, base_dir):
    if 'GATHER_LINKS' in os.environ:
        with open(os.environ['GATHER_LINKS'], 'a') as f:
            for src, dst in links:
                f.write("%s=%s\n" % (src, dst))
    else:
        if os.name == 'nt':
            return
        commands = [
            "ln -sfn %s %s" % (os.path.abspath(os.path.join(str(base_dir), str(src))), str(dst)) for src, dst in links
        ]
        udev = False
        for c in commands:
            c = add_sudo_if_nonroot(c)
            print(c)
            subprocess.check_call(c, shell=True)
            if 'udev' in c:
                udev = True
        if udev:
            c = add_sudo_if_nonroot("service udev restart")
            subprocess.check_call(c, shell=True)


def is_intel_gl():
    try:
        from pyglet.gl import glGetString, GL_VENDOR
    except Exception:
        # No display available
        return False
    vendor_string = cast(glGetString(GL_VENDOR), c_char_p).value.decode()
    return 'intel' in vendor_string.lower()


def install_intel_fix_if_needed():
    if is_intel_gl():
        target_dir = os.path.abspath(os.path.dirname(intel_fix_target))
        mk_dir_call = 'mkdir -p {}'.format(target_dir)
        subprocess.check_call(add_sudo_if_nonroot(mk_dir_call), shell=True)
        call = 'cp {} {}'.format(intel_fix_file, intel_fix_target)
        call = add_sudo_if_nonroot(call)
        subprocess.check_call(call, shell=True)


def uninstall_intel_fix_if_needed():
    if is_intel_gl():
        if os.path.exists(intel_fix_target):
            call = 'rm {}'.format(intel_fix_target)
            call = add_sudo_if_nonroot(call)
            subprocess.check_call(call, shell=True)


def run():
    create_links(LINKS, base)
    install_apt_requirements(APT_REQUIREMENTS, 'psychopy')
