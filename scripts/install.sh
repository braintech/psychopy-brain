#!/usr/bin/env bash
set -e
if grep -Fxq PATH="\$HOME/.local/bin:\$PATH" ~/.profile
then
    echo "Path already in file OK"
else
    echo -e PATH="\$HOME/.local/bin:\$PATH" >> ~/.profile
    REMINDER="************************************\n\nPlease log out and log in\n\n************************************"
fi

sudo su -c 'echo > /etc/udev/rules.d/99-FTDI.rules SUBSYSTEM==\"usb\", ATTR{idVendor}==\"0403\", ATTR{idProduct}==\"6010\", MODE=\"666\"'
sudo service udev restart
sudo apt-get install ffmpeg

echo -e $REMINDER
