#!/bin/bash

set -e

VERSION=`git describe --tags`
LASTTAG=`git describe --tags --first-parent --abbrev=0`
echo $VERSION | tr -d '\n' > version

LICENSE_PATH="psychopy/LICENSE.txt"
LICENSE_TMP_PATH="/tmp/LICENSE.txt"
sed 's/VERSION/'"$VERSION"'/' <$LICENSE_PATH >$LICENSE_TMP_PATH
cp $LICENSE_TMP_PATH $LICENSE_PATH

TEXT="Changes since last tag: "`git log $LASTTAG..HEAD --oneline`
NAME=Braintech
DEBEMAIL=admin@braintech.pl
dch -v $VERSION $TEXT

mk-build-deps -i

fakeroot debian/rules binary
mkdir -p ./dist
mv ../psychopy-brain*.deb ./dist
