# -*- coding: utf-8 -*-
# Part of the PsychoPy+obci library
# Copyright (c) 2017 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.

from os import path

# the absolute path to the folder containing this path
from psychopy.experiment import Param
from psychopy.experiment.components import BaseComponent
from psychopy.tools.versionchooser import _translate

thisFolder = path.abspath(path.dirname(__file__))
iconFile = path.join(thisFolder, "haptic.png")
tooltip = _translate('Haptic: tactile stimulation')


class HapticComponent(BaseComponent):
    categories = ['Stimuli']

    def __init__(self, exp, parentName, name="haptic_1",
                 startType="time (s)", startVal=0.0,
                 stopType="duration (s)", stopVal=1.0,
                 startEstim='', durationEstim=''):
        super(HapticComponent, self).__init__(
            exp, parentName, name=name,
            startType=startType, startVal=startVal,
            stopType=stopType, stopVal=stopVal,
            startEstim=startEstim, durationEstim=durationEstim)

        self.type = "Haptic stimulation"
        self.parentName = parentName
        # params
        self.params['channel'] = Param(
            '1', valType='code', allowedTypes=[],
            updates='set every repeat',
            allowedUpdates=['set every repeat'],
            hint="Index of active channel for haptic stimulator",
            label="haptic channel")

    def writeInitCode(self, buff):
        buff.writeIndented("from psychopy import haptic\n")
        # replaces variable params with sensible defaults
        buff.writeIndented("%s = haptic.HapticEngine()\n" %
                           self.params['name'].val)

    def writeFrameCode(self, buff):
        name = self.params['name'].val
        self.writeStartTestCode(buff)
        buff.writeIndented("%s.start()\n" % name)
        buff.writeIndented("%s.status = STARTED\n" % name)
        buff.setIndentLevel(-1, relative=True)
        self.writeStopTestCode(buff)
        buff.writeIndented("%s.stop()\n" % name)
        buff.writeIndented("%s.status = FINISHED\n" % name)
        buff.setIndentLevel(-1, relative=True)

    def writeExperimentEndCode(self, buff):
        buff.writeIndented("%s.close()\n" % self.params["name"].val)

    def writeRoutineEndCode(self, buff):
        name = self.params['name'].val
        buff.writeIndented("if %s.status == STARTED:\n" % name)
        buff.setIndentLevel(+1, relative=True)
        buff.writeIndented("%s.stop()\n" % name)
        buff.setIndentLevel(-1, relative=True)
