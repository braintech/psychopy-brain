import select
import socket
import time
from datetime import datetime

from psychopy.tsaThermode import int_from_bytes, int_to_bytes, id_to_command


def main():

    HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
    PORT = 20121  # Port to listen on (non-privileged ports are > 1023)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((HOST, PORT))
        s.listen()
        while True:
            try:
                conn, addr = s.accept()
            except:
                continue
            with conn:
                print(f"Connected by {addr}")
                while True:
                    try:
                        msg = data = conn.recv(1024)
                    except socket.timeout:
                        break
                    while data:
                        try:
                            ready = select.select([s], [], [], 0.01)
                            if ready[0]:
                                data = s.recv(1024)
                                msg += data
                            else:
                                break
                        except OSError:
                            break
                    if len(data) == 0:
                        time.sleep(0.001)
                        break

                    command_length = int_from_bytes(msg[:4])
                    timestamp_bytes = msg[4:8]
                    timestamp = int_from_bytes(timestamp_bytes)
                    command_code = int_from_bytes(msg[8:9])
                    command_code_str = bin(command_code).split('b')[1].rjust(8, '0') + " " + id_to_command.get(
                        command_code, "unknown")

                    parameter = None
                    try:
                        parameter = int_from_bytes(msg[9:13])
                    except:
                        pass
                    print(("received command:\n"
                           "comand_length: {}\n"
                          "timestamp: {}, {}\ncommand: {}\nparam: {}").format(command_length, timestamp,
                                                                              datetime.fromtimestamp(timestamp),
                                                                              command_code_str, parameter)
                          )
                    if not data:
                        break
                    response_without_length = timestamp_bytes
                    command_bytes = int_to_bytes(command_code, 1)
                    state_bytes = bytes([0])
                    test_state = bytes([0])
                    respcode = int_to_bytes(0, 2)
                    testtime = bytes([0, 0, 0, 10])
                    temp = int_to_bytes(1234, 2)
                    covas = bytes([1])
                    yes = bytes([1])
                    no = bytes([1])
                    message = "test reponse_text".encode()
                    response_without_length = response_without_length + command_bytes + state_bytes + test_state + respcode + testtime + temp + covas + yes + no + message
                    length = len(response_without_length)
                    response = int_to_bytes(length, 4) + response_without_length

                    conn.sendall(response)
                    print('set response')


if __name__ == '__main__':
    main()

