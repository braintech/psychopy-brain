# PsychoPy

[![Travis-CI status](https://img.shields.io/travis/psychopy/psychopy.svg)](https://travis-ci.org/psychopy/psychopy)
[![Coveralls status](https://img.shields.io/coveralls/psychopy/psychopy.svg)](https://coveralls.io/r/psychopy/psychopy)
[![PyPI version](https://img.shields.io/pypi/v/psychopy.svg)](https://pypi.python.org/pypi/PsychoPy)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg)](code-of-conduct.md)

PsychoPy is an open-source package for creating experiments in behavioral science. It aims to provide a single package that is:

* precise enough for psychophysics
* easy enough for teaching
* flexible enough for everything else
* able to run experiments in a local Python script or online in JavaScript

To meet these goals PsychoPy provides a choice of interface - you can use a
simple graphical user interface called Builder, or write your experiments in
Python code. The entire application and library are written in Python and is
platform independent.

There is a range of documentation at:

* [PsychoPy Homepage](https://www.psychopy.org)
* [Youtube](https://www.youtube.com/playlist?list=PLFB5A1BE51964D587)
* The textbook, [Building Experiments in PsychoPy](https://uk.sagepub.com/en-gb/eur/building-experiments-in-psychopy/book253480)
* [The discourse user forum](https://discourse.psychopy.org)

### Development/Running from source
#### Downloading source code

Using HTTP authentication:

```
git clone https://gitlab.com/braintech/psychopy-brain.git
```

Or if you had configured SSH access to GitLab:

```
git clone git@gitlab.com:braintech/psychopy-brain.git
```
#### Installing dependencies

From repository root directory run:
```
./scripts/install.sh
```

## PsychoPy Brain

Ubuntu or GTK or wxWidgets (it's not clear from bug reports) has a bug, which manifestst itself when clicking on tabs in 'Code' block in PsychoPy:

```
(psychopy:16690): Gdk-CRITICAL **: IA__gdk_window_thaw_updates: assertion 'impl_window->update_freeze_count > 0' failed
```

To circumvent this bug:

Run `sudo apt-get install unity-tweak-tool` then, start the Unity Tweak Tool, and go to Scrolling. Switch Scrollbars to Legacy, and the tabs issue immediately resolves.

Or run:

```
gsettings set com.canonical.desktop.interface scrollbar-mode normal
```

Bugs in issue trackers:

https://bugzilla.gnome.org/show_bug.cgi?id=744527
http://trac.wxwidgets.org/ticket/16795
https://trac.filezilla-project.org/ticket/9708


## Contributions

To contribute, please fork the repository, hack in a feature branch, and send a
pull request.  For more, see [CONTRIBUTING.md](CONTRIBUTING.md)
and the developers documentation at http://www.psychopy.org/developers/developers.html

## More information

* Homepage: https://www.psychopy.org
* Forum: https://discourse.psychopy.org
* Issue tracker: https://github.com/psychopy/psychopy/issues
* Changelog: https://www.psychopy.org/changelog.html
