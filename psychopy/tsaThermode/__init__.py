import select
from datetime import datetime
from time import time, sleep

from psychopy.experiment.components._base import TagParamsDummyHandlerMixin
import socket
import struct


DEBUG = False
TIME_DELAY_FOR_MEDOC = 0.01 # s


def int_to_bytes(integer, nbytes):
    if nbytes == 4:
        integer = socket.htonl(integer)
    if nbytes == 2:
        integer = socket.htons(integer)
    return integer.to_bytes(nbytes, byteorder='big')


def int_from_bytes(xbytes):
    integer = int.from_bytes(xbytes, 'big')
    if len(xbytes) == 4:
        integer = socket.ntohl(integer)
    if len(xbytes) == 2:
        integer = socket.ntohs(integer)
    return integer


test_states = {
    0: 'IDLE',
    1: 'RUNNING',
    2: 'PAUSED',
    3: 'READY'
}

states = {
    0: "IDLE",
    1: "READY",
    2: "TEST IN PROGRESS"
}

response_codes = {0: "OK",
                  1: "FAIL: ILLEGAL PARAMETER",
                  2: "FAIL: ILLEGAL STATE TO SEND THIS",
                  3: "FAIL: NOT THE PROPER TEST STATE",
                  4096: "DEVICE COMMUNICATION ERROR",
                  8192: "safety warning, test continues",
                  16384: "Safety error, going to IDLE"
                  }

command_to_id = {
    'GET_STATUS': 0,
    'SELECT_TP': 1,  # select active program
    'START': 2,
    'PAUSE': 3,
    'TRIGGER': 4,
    'STOP': 5,
    'ABORT': 6,
    'YES': 7,  # used to start increasing the temperature
    'NO': 8,  # used to start decreasing the temperature
    'COVAS': 9,
    'VAS': 10,
    'SPECIFY_NEXT': 11,
    'T_UP': 12,
    'T_DOWN': 13,
    'KEYUP': 14,  # used to stop the temperature gradient,
    'UNNAMED': 15
}
# make the same as above but reversed:
id_to_command = {item:key for key, item in command_to_id.items()}


class MedocResponse:
    """
    A container to interpret and store the output response.
    """
    # decoding the bytes we receive:
    def __init__(self, response):
        self.length = struct.unpack_from('H', response[0:4])[0]
        self.timestamp = int_from_bytes(response[4:8])
        self.datetime = datetime.fromtimestamp(self.timestamp)
        self.strtime = self.datetime.strftime("%Y-%m-%d %H:%M:%S")
        self.command = int_from_bytes(response[8:9])
        self.state = int_from_bytes(response[9:10])
        # see if we have a documented state for this response:
        if self.state in states:
            self.statestr = states[self.state]
        else:
            self.statestr = 'unknown state code'
        self.teststate = int_from_bytes(response[10:11])
        # see if we have a documented test state for this response:
        if self.teststate in test_states:
            self.teststatestr = test_states[self.teststate]
        else:
            self.teststatestr = 'unknown test state code'
        self.respcode = struct.unpack_from('H', response[11:13])[0]
        if self.respcode in response_codes:
            self.respstr = response_codes[self.respcode]
        else:
            self.respstr = "unknown response code"

        # the test time is in seconds once divided by 1000:
        self.testtime = struct.unpack_from('I', response[13:17])[0] / 1000.

        # the temperature is in °C once divided by 100:
        self.temp = struct.unpack_from('h', response[17:19])[0] / 100.
        self.CoVAS = int_from_bytes(response[19:20])
        self.yes = int_from_bytes(response[20:21])
        self.no = int_from_bytes(response[21:22])
        self.message = response[22:self.length].decode()
        # store the whole response
        self.response = response

    def __repr__(self):
        msg = ""
        msg += f"timestamp: {self.strtime}\n"
        msg += "command: {}\n".format(id_to_command.get(self.command, 'Unknown: {}'.format(self.command)))
        msg += f"state  : {self.statestr}\n"
        msg += f"test state : {self.teststatestr}\n"
        msg += f"response code : {self.respstr}\n"
        msg += f"temperature : {self.temp}°C\n"
        if self.statestr == "TEST IN PROGRESS":
            msg += f"test time: {self.testtime} seconds\n"
        elif self.respstr != "OK":
            msg += f"sup. message : {self.message}\n"
        if self.yes:
            msg += "~~ also: yes was pressed! ~~\n"
        if self.no:
            msg += "~~ also: no was pressed! ~~\n"
        msg += f"text message: {self.message}\n"
        return msg

    def __str__(self):
        return self.__repr__()

    def __getitem__(self, s):
        return self.response[s]


class ThermodeConnectionEngine(TagParamsDummyHandlerMixin):
    command_error_string = ("Commands should 8 bit binary program code from MMS software or one"
                            " of the commands below:\n{}".format(', '.join(command_to_id.keys()))
                            )

    def __init__(self, address='127.0.0.1', port=20121):
        self.address = address
        self.port = port
        self.response = None
        self._comnand = None
        self._parameter = None
        self._sent = False
        self.status = None
        self._raise_exceptions = True

    def setRaise_exceptions(self, x):
        self._raise_exceptions = bool(x)

    def setAddress(self, x):
        self.address = x

    def setAport(self, x):
        self.port = int(x)

    def setCommand(self, x):
        self._command = x

    def setMmsparam(self, x=None):
        self._parameter = x

    def _build_command(self, command_id, parameter_value=None):
        if isinstance(command_id, str):
            if command_id.upper() in command_to_id:
                command_code = command_to_id[command_id.upper()]
            elif len(command_id) == 8:
                try:
                    command_code = int(command_id, 2)
                except ValueError:
                    raise ValueError(self.command_error_string)
            else:
                raise ValueError(self.command_error_string)
        elif isinstance(command_id, int):
            command_code = command_id
        else:
            raise ValueError(self.command_error_string)

        if isinstance(parameter_value, str) and len(parameter_value)==8:
        # then program code, e.g. '00000001'
            parameter = int(parameter_value, 2)
        elif isinstance(parameter_value, (float, int)):
            parameter = 100 * parameter_value
        elif parameter_value is None:
            parameter = None
        else:
            raise ValueError("Wrong parameter value. It should be either 8 character string (for program selection)"
                             " or float for ")

        commandbytes = int_to_bytes(int(time()), 4)
        commandbytes += int_to_bytes(int(command_code), 1)
        if parameter is not None:
            commandbytes += int_to_bytes(int(parameter), 4)
        # final command format - 4 byte int with command length, byte - program code, another 2 byte int parameter
        # value, temperature is multiplied by 100, so 4000 would be 40 degrees celsius.
        commandbytes = int_to_bytes(len(commandbytes), 4) + commandbytes
        return commandbytes

    def send_command(self, command_id, parameter_value):
        commandbytes = self._build_command(command_id, parameter_value)
        for attempts in range(3):
            try:
                s = socket.socket()
                s.connect((self.address, self.port))
                s.send(commandbytes)
                data = msg = s.recv(1024)
                while data:
                    ready = select.select([s], [], [], 0.001)
                    if ready[0]:
                        data = s.recv(1024)
                        msg += data
                    else:
                        break
                resp = MedocResponse(msg)
                if resp.respcode != 0:
                    exc_text = "Command {} failed, error code: {}, MMS software response:\n{}".format(resp.command,
                                                                                                      resp.respstr,
                                                                                                      resp)
                    if self._raise_exceptions:
                        raise Exception(exc_text)
                    else:
                        print(exc_text)
                self.response = resp
                if DEBUG:
                    print("Received: ")
                    print(resp)
                return resp
            except ConnectionResetError:
                print("==> ConnectionResetError")
                s.close()
            return resp

    def start(self):
        if self._sent == False:
            self._sent = True
            return self.send_command(self._command, self._parameter)
        return self.response

    def stop(self):
        self._sent = False
