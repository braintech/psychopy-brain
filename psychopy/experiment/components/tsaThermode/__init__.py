# -*- coding: utf-8 -*-
# Part of the PsychoPy+obci library
# Copyright (c) 2022 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.

from os import path

# the absolute path to the folder containing this path
from psychopy.experiment import Param
from psychopy.experiment.components import BaseComponent
from psychopy.tools.versionchooser import _translate
from psychopy.tsaThermode import ThermodeConnectionEngine

thisFolder = path.abspath(path.dirname(__file__))
iconFile = path.join(thisFolder, "thermode.png")
tooltip = _translate('TSA 2 Thermoe: pain/temperature stimulation external controll')


class TsaThermodeComponent(BaseComponent):
    categories = ['Stimuli']

    def __init__(self, exp, parentName, name="thermode_1",
                 startType="time (s)", startVal=0.0,
                 stopType="duration (s)", stopVal=1.0,
                 startEstim='', durationEstim=''):
        super(TsaThermodeComponent, self).__init__(
            exp, parentName, name=name,
            startType=startType, startVal=startVal,
            stopType=stopType, stopVal=stopVal,
            startEstim=startEstim, durationEstim=durationEstim)
        self.type = "TSA 2 Thermode External Control"
        self.parentName = parentName
        # params
        self.params['address'] = Param(
            '"127.0.0.1"', valType='code', allowedTypes=[],
            updates='set every repeat',
            allowedUpdates=['set every repeat'],
            hint="IP address/hostname of the MMS software ie 127.0.0.1",
            label="IP")
        # aport, because window has parameters in alphabetical order
        self.params['aport'] = Param(
            20121, valType='num', allowedTypes=[],
            updates='set every repeat',
            allowedUpdates=['set every repeat'],
            hint="Port for the MMS software",
            label="port")

        self.params['command'] = Param(
            '"start"', valType='code', allowedTypes=[],
            updates='set every repeat',
            allowedUpdates=['set every repeat'],
            hint=ThermodeConnectionEngine.command_error_string,
            label="Command to send to MMS software")

        self.params['mmsparam'] = Param(
            '', valType='code', allowedTypes=[],
            updates='set every repeat',
            allowedUpdates=['set every repeat'],
            hint="Accepts int, float, for temperature, binary str for program code",
            label="Parameter for the command to send to MMS software")

        self.params['raise_exceptions'] = \
            Param(val=True, valType='bool',
                  updates='set every repeat',
                  allowedUpdates=['set every repeat'],
                  hint="In case of communication errors fail silently and continue or raise an exception",
                  label="Raise exceptions"
                  )

    def writeInitCode(self, buff):
        buff.writeIndented("from psychopy.tsaThermode import ThermodeConnectionEngine\n")
        buff.writeIndented("%s = ThermodeConnectionEngine()\n" %
                           self.params['name'].val)

    def writeFrameCode(self, buff):
        name = self.params['name'].val
        self.writeStartTestCode(buff)
        buff.writeIndented("%s.start()\n" % name)
        buff.writeIndented("%s.status = STARTED\n" % name)
        buff.setIndentLevel(-1, relative=True)
        self.writeStopTestCode(buff)
        buff.writeIndented("%s.stop()\n" % name)
        buff.writeIndented("%s.status = FINISHED\n" % name)
        buff.setIndentLevel(-2, relative=True)

    def writeRoutineEndCode(self, buff):
        name = self.params['name'].val
        buff.writeIndented("if %s.status == STARTED:\n" % name)
        buff.setIndentLevel(+1, relative=True)
        buff.writeIndented("%s.stop()\n" % name)
        buff.setIndentLevel(-1, relative=True)
