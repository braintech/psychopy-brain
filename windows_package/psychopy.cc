#include <stdlib.h>
#include <string>       // std::string
#include <stdio.h>
#include <iostream>     // std::cout
#include <sstream>      // std::ostringstrea
#include <Shlwapi.h>
#include <windows.h>

int main(int argc, char **argv){
    char Filename[MAX_PATH]; //this is a char buffer
    GetModuleFileNameA(NULL, Filename, sizeof(Filename));

    std::string filename_s(Filename);
    std::string dirname(filename_s, 0, filename_s.length() -  12);
    std::ostringstream s;
    s << "\"";
    s << dirname;
    s << "..\\python.exe\"  ";
    s << "\"";
    s << dirname;
    s << "psychopy-script.py\" ";
    for (int i = 1; i < argc; i++)
    {
        s << " " << argv[i];
    }

    std::cout << s.str();

    char commandline[MAX_PATH];
    strcpy(commandline,  s.str().c_str());

    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

    // Start the child process.
    if( !CreateProcess( NULL,   // No module name (use command line)
        commandline,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    )
    {
        printf( "CreateProcess failed (%d).\n", GetLastError() );
        return 1;
    }

    // Wait until child process exits.
    WaitForSingleObject(pi.hProcess, INFINITE );

    // Close process and thread handles.
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
    return 0;


}
