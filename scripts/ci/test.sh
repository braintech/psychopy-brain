#!/bin/bash
set -e
export USER=nobody 
export TRAVIS=true

filename="$PWD/.out"

xvfb-run -s "-screen 0 1024x768x24 -ac +extension GLX +render -noreset" \
	python3.6 psychopy/tests/runPytest.py -v -m "not needs_sound" $@ \
	| tee "$filename"

exit_code=`grep -qe "failed|error" "$filename"`

rm "$filename"

exit $exit_code
