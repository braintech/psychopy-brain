# -*- coding: utf-8 -*-
# Part of the PsychoPy+obci library
# Copyright (c) 2017 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.
import warnings

import time
from collections import namedtuple

from serial import SerialException

from psychopy.experiment.components._base import TagParamsDummyHandlerMixin

try:
    from braintech.drivers.blinker.blinker import Blinker
except ImportError as e:
    msg = r'''
    Cannot import blinker api:
    %s
    ''' % e
    raise ImportError(msg)

_BlinkerDefaults = namedtuple(
    'BlinkerDefaults',
    [
        'function',
        'duty_cycle',
        'frequencies',
        'phases',
        'synchro_field'
    ]
)

BLINKER_DEFAULTS_STR = _BlinkerDefaults(
    function='rect',
    duty_cycle=50,
    # These values are stored as strings and then evaluated using eval
    # for better readability:
    frequencies='[5,5,5,5, 5,5,5,5, 5,5,5,5, 5,5,5,5]',
    phases='[0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0]',
    synchro_field='1'
)


class BlinkerEngine(TagParamsDummyHandlerMixin):
    __BLINKERS = {}

    @classmethod
    def createBlinker(cls, port=None, preview=False):
        if preview and port in cls.__BLINKERS:
            return cls.__BLINKERS[port]
        elif preview:
            instance = Blinker(port, preview)
            cls.__BLINKERS[port] = instance
            return instance
        else:
            return Blinker(port, preview)

    def __init__(self, port, preview):
        self.status = None
        try:
            self._blinker = self.createBlinker(port, preview)
        except SerialException:
            warnings.warn('Physical blinker at port {} not connected, using preview'.format(port))
            self._blinker = self.createBlinker(port='dummy_fat16_blinker', preview=True)
        self._config_duration = 0
        self._fun = BLINKER_DEFAULTS_STR.function
        self._duty = BLINKER_DEFAULTS_STR.duty_cycle
        self._freqs = eval(BLINKER_DEFAULTS_STR.frequencies)
        self._phases = eval(BLINKER_DEFAULTS_STR.phases)
        self._synchro_field = int(BLINKER_DEFAULTS_STR.synchro_field)
        self._blinker.stop()

    def configure(self):
        t0 = time.time()
        max_luminosity = 1.0
        min_luminosity = 0.0
        for i in range(len(self._freqs)):
            self._blinker.enable(i)
            if self._freqs[i] == 0:
                self._blinker.set_still(i, max_luminosity)
            elif self._freqs[i] == -1:
                self._blinker.set_still(i, min_luminosity)
            elif self._fun == 'sin':
                self._blinker.set_sine_function(i, self._freqs[i], self._phases[i], max_luminosity)
            elif self._fun == 'rect':
                self._blinker.set_rectangular_function(
                    i, self._freqs[i], self._duty, self._phases[i], max_luminosity
                )
            else:
                raise ValueError
        self._blinker.set_synchro_field(self._synchro_field - 1)
        self._config_duration = time.time() - t0

    def configDuration(self):
        return self._config_duration

    def setFun(self, fun):
        self._fun = fun

    def setDuty(self, duty):  # 0 - 100.0
        self._duty = duty / 100.0  # API expects 0.0 - 1.0

    def setFreqs(self, freqs):
        self._freqs = freqs

    def setPhases(self, phases):
        self._phases = phases

    def setSynchro_field(self, synchro):
        self._synchro_field = synchro

    def start(self):
        self._blinker.start()

    def stop(self):
        self._blinker.stop()
        for field in range(self._blinker.FIELDS_NUMBER):
            self._blinker.disable(field, 1.0)
        self._blinker.flush()

    def close(self):
        self._blinker.clear()
        self._blinker.stop()
        self._blinker.close()
