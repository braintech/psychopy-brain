from devops import devpi
from devops.devpi import _get_package_version, _index_arg
from fabric import context_managers, api
from devops.ci import install


def _push_no_delete(path, index, from_index=None):
    package, version = _get_package_version(path)
    with context_managers.lcd(path):
        api.local("devpi push {package}=={version} {index} {from_index}".format(
            package=package, index=index, version=version, from_index=_index_arg(from_index)
        ))


def deploy_packages_to_installer():
        devpi_index = devpi.current_index()['name']
        from_index = devpi.temp_name(devpi_index)
        _push_no_delete('.', 'obci/installers', from_index)


def build_psychopy_deb():
    api.local("python3 deb_package/psychopy.py")
    api.local("mkdir -p dist")
    api.local("mv deb_package/build/psychopy*/*.deb dist")
    install.sign_debs('dist')
