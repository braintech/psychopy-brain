# -*- coding: utf-8 -*-
# Part of the PsychoPy+obci library
# Copyright (c) 2017 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.

from os import path
from psychopy.experiment import Param
from psychopy.experiment.components import BaseComponent
from psychopy.tools.versionchooser import _translate
from psychopy.blinker import BLINKER_DEFAULTS_STR

# the absolute path to the folder containing this path
thisFolder = path.abspath(path.dirname(__file__))
iconFile = path.join(thisFolder, "blinker.png")
tooltip = _translate('Blinker: SSVEP stimulation')


class BlinkerComponent(BaseComponent):
    categories = ['Stimuli']

    def __init__(self, exp, parentName, name="blinker_1",
                 startType="time (s)", startVal=0.0,
                 stopType="duration (s)", stopVal=1.0,
                 startEstim='', durationEstim=''):
        super(BlinkerComponent, self).__init__(
            exp, parentName, name=name,
            startType=startType, startVal=startVal,
            stopType=stopType, stopVal=stopVal,
            startEstim=startEstim, durationEstim=durationEstim)
        self.order = ['fun', 'duty', 'freqs', 'phases', 'port', 'preview']
        self.type = "Blinker stimulation"
        self.parentName = parentName

        # params
        self.params['fun'] = Param(
            BLINKER_DEFAULTS_STR.function, valType='str', allowedTypes=[],
            # allowedVals=['sin', 'rect'],
            updates='constant',
            allowedUpdates=['constant', 'set every repeat'],
            hint="rect or sin",
            label="Stimulating function")
        self.params['duty'] = Param(
            BLINKER_DEFAULTS_STR.duty_cycle, valType='code', allowedTypes=[],
            updates='constant',
            allowedUpdates=['constant', 'set every repeat'],
            hint="0 to 100",
            label="Duty cycle")
        self.params['freqs'] = Param(
            BLINKER_DEFAULTS_STR.frequencies, valType='code', allowedTypes=[],
            updates='constant',
            allowedUpdates=['constant', 'set every repeat'],
            hint="list of 16 numbers (Hz)",
            label="Stimulating frequencies")
        self.params['phases'] = Param(
            BLINKER_DEFAULTS_STR.phases, valType='code', allowedTypes=[],
            updates='constant',
            allowedUpdates=['constant', 'set every repeat'],
            hint="list of 16 numbers (0 - 360)",
            label="Stimulating phases")
        self.params['synchro_field'] = Param(
            BLINKER_DEFAULTS_STR.synchro_field, valType='code', allowedTypes=[],
            updates='constant',
            allowedUpdates=['constant', 'set every repeat'],
            hint="Field index to be duplicated on synchro output, counting from 1",
            label="Synchro output field")
        self.params['port'] = Param(
            "/dev/fat16_blinker", valType='str', allowedTypes=[],
            updates='constant',
            allowedUpdates=['constant'],
            hint="/dev/fat16_blinker or empty",
            categ="Advanced",
            label="Device port")
        self.params['preview'] = Param(
            '1', valType='code', allowedTypes=[],
            updates='constant',
            allowedUpdates=['constant'],
            hint="1 or 0",
            categ="Advanced",
            label="Show preview")

    def __val(self, name):
        param = self.params[name]
        if param.valType == 'code':
            return param.val
        else:
            assert param.valType == 'str'
            if param.val.startswith('$'):
                return param.val[1:]
            else:
                return repr(param.val)

    def writeInitCode(self, buff):
        val = self.__val
        buff.writeIndented("from psychopy import blinker\n")
        buff.writeIndented("%s = blinker.BlinkerEngine(%s, %s)\n" %
                           tuple([val(n) for n in ['name', 'port', 'preview']]))

    def writeRoutineStartCode(self, buff):
        for v in 'fun', 'duty', 'freqs', 'phases', 'synchro_field':
            buff.writeIndented("%s.set%s(%s)\n" % (self.params['name'], v.capitalize(), self.__val(v)))
        buff.writeIndented("{}.configure()\n".format(self.params['name'].val))
        buff.writeIndented("routineTimer.add({}.configDuration())\n".format(self.params['name'].val))
        buff.writeIndented("{}Clock.add({}.configDuration())\n".format(self.parentName, self.params['name'].val))

    def writeFrameCode(self, buff):
        name = self.params['name'].val
        self.writeStartTestCode(buff)
        buff.writeIndented("%s.start()\n" % name)
        buff.writeIndented("%s.status = STARTED\n" % name)
        buff.setIndentLevel(-1, relative=True)
        self.writeStopTestCode(buff)
        buff.writeIndented("%s.stop()\n" % name)
        buff.writeIndented("%s.status = FINISHED\n" % name)
        buff.setIndentLevel(-2, relative=True)

    def writeExperimentEndCode(self, buff):
        buff.writeIndented("%s.close()\n" % self.params["name"].val)

    def writeRoutineEndCode(self, buff):
        name = self.params['name'].val
        buff.writeIndented("if %s.status == STARTED:\n" % name)
        buff.setIndentLevel(+1, relative=True)
        buff.writeIndented("%s.stop()\n" % name)
        buff.setIndentLevel(-1, relative=True)
