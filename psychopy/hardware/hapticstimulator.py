import threading
from pyftdi.ftdi import Ftdi

"""
Contains HapticStimulator class to control FTDI FT2232HL in bitbang mode
as haptic stimulator. Tested on pyftdi 0.11.3 and pyusb 1.0.0.b2.
It requires the user to have access to usb device.
On systems with udev: add file /etc/udev/rules.d/99-FTDI.rules
with line (idVendor and idProduct might be different for your stimulator):
SUBSYSTEM=="usb", ATTR{idVendor}=="0403", ATTR{idProduct}=="6010", MODE="666"
"""

VENDORID = 0x0403
PRODUCTID = 0x6010
DEFAULTINTERFACE = 2  # Interface to control
DEFAULTDIRECTION = 0xFF  # All pins - output


class HapticStimulator:
    def __init__(self, vendorid=VENDORID, productid=PRODUCTID,
                 interface=DEFAULTINTERFACE):
        """
        Class to initialise and send commands in bitbang mode to the haptic
        stimulator built on FTDI FT2232HL with power transistors board
        ("fMRI Pneumatic 5V version") from Politechnika Warszawska.

        :param vendorid: int or hex NOT string
        gotten from lsusb for FTDI device
        :param productid: int or hex NOT string
        gotten from lsusb for FTDI device
        :param interface: int - for tested board is always 2
        """
        self.count_per_channel = {}
        self.ftdi = Ftdi()
        try:
            # direction FF - all ports output
            self.ftdi.open_bitbang(vendorid, productid, interface,
                                   direction=0xFF)
            self.ftdi.write_data([0])  # turn off all channels
        except Exception:
            # prevent destructor from interacting with FTDI
            self.ftdi = None
            raise

    def start(self, chnl):
        """
        Turn on stimulation on channel number chnl.

        :param chnl: integer - channel number starting from 1
        """
        if self.ftdi is not None:
            if chnl not in self.count_per_channel:
                apins = self.ftdi.read_pins()
                # create byte with bit on corresponding channel enabled
                activation_mask = 1 << (chnl - 1)
                # add active bit to other active channels
                self.ftdi.write_data([apins | activation_mask])
                self.count_per_channel[chnl] = 1
            else:
                self.count_per_channel[chnl] += 1

    def stop(self, chnl):
        """
        Turn off stimulation on channel number chnl.

        :param chnl: int - number of channel to turn off
        """
        if self.ftdi is not None and chnl in self.count_per_channel:
            if self.count_per_channel[chnl] == 1:
                apins = self.ftdi.read_pins()
                # select only needed channel
                activation_mask = ~(1 << (chnl - 1)) & 0xFF
                self.ftdi.write_data([apins & activation_mask])
                del self.count_per_channel[chnl]
            else:
                self.count_per_channel[chnl] -= 1

    def close(self):
        """
        Release the device.
        """
        if self.ftdi is not None:
            self.ftdi.write_data([0])  # turn off all channels
            self.ftdi.usb_dev.reset()  # release device
            self.ftdi = None

    def __del__(self):
        self.close()
