# -*- coding: utf-8 -*-
# Part of the PsychoPy+obci library
# Copyright (c) 2017 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.
from psychopy.experiment.components._base import TagParamsDummyHandlerMixin

try:
    from psychopy.hardware.hapticstimulator import HapticStimulator
except ImportError as e:
    msg = r'''
    You have missing libraries: pyftdi and pyusb,
    to install run:
    sudo apt-get install python-usb
    sudo pip install pyftdi
    '''
    raise ImportError(msg)

try:
    _stimulator = HapticStimulator()
except Exception as e:
    raise Exception('haptic stimulator device could not be found')

class HapticEngine(TagParamsDummyHandlerMixin):
    def __init__(self):
        self.status = None
        self.channel = None

    def setChannel(self, chnl):
        self.channel = int(chnl)

    def start(self):
        if self.channel is not None:
            _stimulator.start(self.channel)

    def stop(self):
        if self.channel is not None:
            _stimulator.stop(self.channel)

    def close(self):
        _stimulator.close()
