import os.path
import sys
import py.path
import subprocess


def test_psyexp_to_python_compilation(tmpdir):
    output_path = tmpdir.join('experiment_as_script.py')
    psyexp_path = hello_world_psychopy_experiment_path()
    subprocess.check_call(
        ['psyexp_to_python', psyexp_path, '-o', str(output_path)],
    )
    assert output_path.check(file=True, exists=True)
    subprocess.check_call([sys.executable, str(output_path)])
    created_script = output_path.read()
    assert "filename = " in created_script
    assert "core.quit()" in created_script


def test_psyexp_to_python_stdout():
    psyexp_path = hello_world_psychopy_experiment_path()
    created_script = subprocess.check_output(
        ['psyexp_to_python', psyexp_path],
    ).decode()
    assert "filename = " in created_script
    assert "core.quit()" in created_script


def hello_world_psychopy_experiment_path():
    path = os.path.join(resources_path(), 'hello_world.psyexp')
    experiment = py.path.local(path)
    assert experiment.check(file=True)
    assert "<PsychoPy2experiment" in experiment.read()
    return path


def resources_path():
    return os.path.join(this_script_dir(), 'resources')


def this_script_dir():
    return os.path.dirname(os.path.abspath(__file__))
